﻿using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.Application;
using log4net;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace JondellCorp.UnitTest.ServiceTests
{
    /// <summary>
    /// Application service base : test class method - unit testing
    /// </summary>
    [TestFixture]
    public class ApplicationServiceTest
    {
        private Mock<IJondellCorpDbContext> _jondellCorpDbContext;
        private Mock<ILog> _logger;

        private ApplicationService _applicationService;

        [SetUp]
        public void SetUp()
        {
            _jondellCorpDbContext = new Mock<IJondellCorpDbContext>();
            _logger = new Mock<ILog>();

            _applicationService = new ApplicationService(_jondellCorpDbContext.Object, _logger.Object);
        }


        [Test]
        public void GetApplicationAsync_RecordExists_ApplicationName()
        {
            //Arrange
            IQueryable<Application> mockApplication =
            new List<Application>
            {
                new Application { Id = 1, Name = "jondell" },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Application>>();
            mockSet.As<IQueryable<Application>>().Setup(m => m.Provider).Returns(mockApplication.Provider);
            mockSet.As<IQueryable<Application>>().Setup(m => m.Expression).Returns(mockApplication.Expression);
            mockSet.As<IQueryable<Application>>().Setup(m => m.ElementType).Returns(mockApplication.ElementType);
            mockSet.As<IQueryable<Application>>().Setup(m => m.GetEnumerator()).Returns(mockApplication.GetEnumerator());

            _jondellCorpDbContext.Setup(c => c.Application).Returns(mockSet.Object);

            //Act
            Application result = _applicationService.GetApplication();

            //Assert
            NUnit.Framework.Assert.AreEqual("jondell", result.Name);
        }
    }
}
