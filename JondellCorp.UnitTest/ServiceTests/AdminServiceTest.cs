﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.Admin;
using log4net;
using Moq;
using NUnit.Framework;

namespace JondellCorp.UnitTest.ServiceTests
{
    /// <summary>
    /// Summary description for AdminServiceTest
    /// </summary>
    [TestFixture]
    public class AdminServiceTest
    {
        private Mock<IJondellCorpDbContext> _jondellCorpDbContext;
        private Mock<ILog> _logger;

        private AdminService _adminService;

        [SetUp]
        public void SetUp()
        {
            _jondellCorpDbContext = new Mock<IJondellCorpDbContext>();
            _logger = new Mock<ILog>();

            _adminService = new AdminService(_jondellCorpDbContext.Object, _logger.Object);
        }


        [Test]
        public void ProcessFileAsync_UpdateExistingRecord_ReturnSuccess()
        {
            //Arrange
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "TestData\\TestDataFile-December-2016.txt";
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            FileUploadModel fileUploadModel = new FileUploadModel()
            {
                Year = "2016",
                Month = (Month)Enum.Parse(typeof(Month), "December"),
                FileStream = fileStream,
                ContentType = "text/plain",
                FileName = "TestDataFile-December-2016.txt",
                UserName = "dev",
            };

            IQueryable<AccountHistory> mockApplication =
            new List<AccountHistory>
            {
                new AccountHistory { Year = 2016, Month = 12 },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<AccountHistory>>();
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.Provider).Returns(mockApplication.Provider);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.Expression).Returns(mockApplication.Expression);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.ElementType).Returns(mockApplication.ElementType);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.GetEnumerator()).Returns(mockApplication.GetEnumerator());

            _jondellCorpDbContext.Setup(c => c.AccountHistory).Returns(mockSet.Object);

            //Act
            string result = _adminService.ProcessFile(fileUploadModel);

            //Assert
            NUnit.Framework.Assert.AreEqual("Success", result);
        }

        [Test]
        public void ProcessFileAsync_InsertNewRecord_ReturnSuccess()
        {
            //Arrange
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "TestData\\TestDataFile-December-2016.txt";
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            FileUploadModel fileUploadModel = new FileUploadModel()
            {
                Year = "2016",
                Month = (Month)Enum.Parse(typeof(Month), "December"),
                FileStream = fileStream,
                ContentType = "text/plain",
                FileName = "TestDataFile-December-2016.txt",
                UserName = "dev",
            };

            IQueryable<AccountHistory> mockApplication =
            new List<AccountHistory>
            {
                new AccountHistory { },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<AccountHistory>>();
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.Provider).Returns(mockApplication.Provider);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.Expression).Returns(mockApplication.Expression);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.ElementType).Returns(mockApplication.ElementType);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.GetEnumerator()).Returns(mockApplication.GetEnumerator());

            _jondellCorpDbContext.Setup(c => c.AccountHistory).Returns(mockSet.Object);

            //Act
            string result = _adminService.ProcessFile(fileUploadModel);

            //Assert
            NUnit.Framework.Assert.AreEqual("Success", result);
        }

        [Test]
        public void ProcessFileAsync__AccountBallanceMissingInTheFile_ReturnErrorMessage()
        {
            //Arrange
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "TestData\\ErrorTestDataFile-December-2016.txt";
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);

            FileUploadModel fileUploadModel = new FileUploadModel()
            {
                Year = "2016",
                Month = (Month)Enum.Parse(typeof(Month), "December"),
                FileStream = fileStream,
                ContentType = "text/plain",
                FileName = "ErrorTestDataFile-December-2016.txt",
                UserName = "dev",
            };

            IQueryable<AccountHistory> mockApplication =
            new List<AccountHistory>
            {
                new AccountHistory { },
            }.AsQueryable();

            var mockSet = new Mock<DbSet<AccountHistory>>();
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.Provider).Returns(mockApplication.Provider);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.Expression).Returns(mockApplication.Expression);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.ElementType).Returns(mockApplication.ElementType);
            mockSet.As<IQueryable<AccountHistory>>().Setup(m => m.GetEnumerator()).Returns(mockApplication.GetEnumerator());

            _jondellCorpDbContext.Setup(c => c.AccountHistory).Returns(mockSet.Object);

            //Act
            string result = _adminService.ProcessFile(fileUploadModel);

            //Assert
            NUnit.Framework.Assert.AreEqual("Error reading file", result);
        }
    }
}
