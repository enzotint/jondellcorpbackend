﻿using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.User;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace JondellCorp.Controllers
{
    /// <summary>
    /// User managment
    /// </summary>
    public class UserController : ApiController
    {
        private IUserService _userService;

        /// <summary>
        /// constructor : Dependancy injection
        /// </summary>
        /// <param name="userService"></param>
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Get user roles
        /// </summary>
        /// <returns>User</returns>
        [Route("api/roles")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<List<UserRoleModel>> GetRoles()
        {
            List<UserRoleModel> result = await _userService.GetUserRolesAsync();
            return result;
        }

        /// <summary>
        /// Get user
        /// </summary>
        /// <returns>User</returns>
        [Route("api/user")]
        [HttpGet]
        public UserModel GetUser()
        {
            // TODO : moove to service
            var identityClaims = (ClaimsIdentity)User.Identity;
            IEnumerable<Claim> claims = identityClaims.Claims;
            UserModel result = new UserModel()
            {
                UserName = identityClaims.FindFirst("UserName").Value,
                Email = identityClaims.FindFirst("Email").Value,
                LoggedOn = identityClaims.FindFirst("LoggedOn").Value
            };
            return result;
        }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Identity Result</returns>
        [Route("api/user/signup")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IdentityResult> SignupAsync(UserModel model)
        {
            IdentityResult result = await _userService.CreateUserAsync(model);
            return result;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("api/ForAdminRole")]
        public string ForAdminRole()
        {
            return "for admin role";
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        [Route("api/ForUserRole")]
        public string ForAuthorRole()
        {
            return "For user role";
        }

        [HttpGet]
        [Authorize(Roles = "Admin,User")]
        [Route("api/ForAdminorUserr")]
        public string ForAuthorOrReader()
        {
            return "For admin/user role";
        }
    }
}
