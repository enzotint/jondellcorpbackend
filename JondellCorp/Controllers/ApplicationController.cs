﻿using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.Application;
using System.Web.Http;

namespace JondellCorp.Controllers
{
    /// <summary>
    /// Application service(s) test
    /// </summary>
    public class ApplicationController : ApiController
    {
        private IApplicationService _applicationService;

        /// <summary>
        /// constructor : Dependancy injection
        /// </summary>
        /// <param name="applicationService"></param>
        public ApplicationController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        /// <summary>
        /// Check database connection and master data selection
        /// </summary>
        /// <returns>Application</returns>
        [Route("api/application")]
        [HttpGet]
        [AllowAnonymous]
        public Application Get()
        {
            JondellCorp.EndSystem.Models.Application result = _applicationService.GetApplication();
            return result;
        }
    }
}
