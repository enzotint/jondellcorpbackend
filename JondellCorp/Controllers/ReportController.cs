﻿using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.Report;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace JondellCorp.Controllers
{
    public class ReportController : ApiController
    {
        private IReportService _reportService;

        /// <summary>
        /// constructor : Dependancy injection
        /// </summary>
        /// <param name="userService"></param>
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        /// <summary>
        /// Get account overview
        /// </summary>
        /// <returns>User</returns>
        [Route("api/report")]
        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<List<AccountHistory>> GetReport(string fromYear, string toYear, string fromMonth, string toMonth)
        {
            List<AccountHistory> result = await _reportService.GetReportAsync(fromYear, toYear, fromMonth, toMonth);
            return result;
        }
    }
}
