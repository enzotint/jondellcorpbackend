﻿using System.Web.Mvc;

namespace JondellCorp.Controllers
{
    /// <summary>
    /// MVC Controller - Service running information
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// return application running status
        /// </summary>
        /// <returns>index view</returns>
        public ActionResult Index()
        {
            ViewBag.Title = "Jondell Corp";
            return View();
        }
    }
}
