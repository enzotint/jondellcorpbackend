﻿using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.Admin;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace JondellCorp.Controllers
{
    public class AdminController : ApiController
    {
        private IAdminService _adminService;

        /// <summary>
        /// constructor : Dependancy injection
        /// </summary>
        /// <param name="userService"></param>
        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        /// <summary>
        /// Upload file
        /// </summary>
        /// <param name="model"></param>
        /// <returns>None</returns>
        [Route("api/admin/upload")]
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public string UploadFile()
        {
            HttpRequest httpRequest = HttpContext.Current.Request;
            var identityClaims = (ClaimsIdentity)User.Identity;

            FileUploadModel model = new FileUploadModel()
            {
                Year = httpRequest["year"],
                Month = (Month)Enum.Parse(typeof(Month), httpRequest["month"]),
                FileStream = httpRequest.Files["uploadFile"].InputStream,
                ContentType = httpRequest.Files["uploadFile"].ContentType,
                FileName = httpRequest.Files["uploadFile"].FileName,
                UserName = identityClaims.FindFirst("UserName").Value,
            };

            string message = _adminService.ProcessFile(model);
            return message;
        }
    }
}
