﻿using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.Overview;
using System.Threading.Tasks;
using System.Web.Http;

namespace JondellCorp.Controllers
{
    public class OverviewController : ApiController
    {
        private IOverviewService _overviewService;

        /// <summary>
        /// constructor : Dependancy injection
        /// </summary>
        /// <param name="userService"></param>
        public OverviewController(IOverviewService overviewService)
        {
            _overviewService = overviewService;
        }

        /// <summary>
        /// Get account overview
        /// </summary>
        /// <returns>User</returns>
        [Route("api/overview")]
        [Authorize(Roles = "User")]
        [HttpGet]
        public async Task<AccountHistory> GetAccount(string year, string month)
        {
            AccountHistory result = await _overviewService.GetAccountAsync(year, month);
            return result;
        }
    }
}
