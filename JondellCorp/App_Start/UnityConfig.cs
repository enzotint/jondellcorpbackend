using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using JondellCorp.EndSystem.Services.Application;
using JondellCorp.EndSystem.Services.User;
using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Services.Admin;
using JondellCorp.EndSystem.Services.Overview;
using JondellCorp.EndSystem.Services.Report;
using log4net;
using JondellCorp.EndSystem.Services.Logger;

namespace JondellCorp
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<ILog>(new InjectionFactory(factory => new LoggerInjection()));

            container.RegisterType<IJondellCorpDbContext, JondellCorpDbContext>();
            container.RegisterType<IApplicationService, ApplicationService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IAdminService, AdminService>();
            container.RegisterType<IOverviewService, OverviewService>();
            container.RegisterType<IReportService, ReportService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}