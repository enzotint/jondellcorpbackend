﻿using JondellCorp.EndSystem.Models;

namespace JondellCorp.EndSystem.Services.File
{
    public abstract class File
    {
        public abstract AccountHistory ReadFile(FileUploadModel model);
    }
}
