﻿using JondellCorp.EndSystem.Models;

namespace JondellCorp.EndSystem.Services.File
{
    public static class FileFactory
    {
        public static File Get(FileType fileType)
        {
            switch (fileType)
            {
                case FileType.text:
                default:
                    return new TextFile();
                case FileType.excel:
                    return new ExcelFile();
            }
        }
    }
}
