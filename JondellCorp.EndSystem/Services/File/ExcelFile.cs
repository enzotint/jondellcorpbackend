﻿using ExcelDataReader;
using JondellCorp.EndSystem.Models;
using System.Data;

namespace JondellCorp.EndSystem.Services.File
{
    public class ExcelFile : File
    {
        public override AccountHistory ReadFile(FileUploadModel model)
        {
            AccountHistory accountHistory = new AccountHistory
            {
                UploadedFileName = model.FileName,
                UploadedFileType = model.ContentType
            };

            using (IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(model.FileStream))
            {
                DataSet result = excelReader.AsDataSet();
                result = excelReader.AsDataSet();

                // TODO : Improve reading by account name without reading from file record order
                accountHistory.RD = decimal.Parse(result.Tables[0].Rows[1][1].ToString());
                accountHistory.Canteen = decimal.Parse(result.Tables[0].Rows[2][1].ToString());
                accountHistory.CEOCar = decimal.Parse(result.Tables[0].Rows[3][1].ToString());
                accountHistory.Marketing = decimal.Parse(result.Tables[0].Rows[4][1].ToString());
                accountHistory.ParkingFines = decimal.Parse(result.Tables[0].Rows[5][1].ToString());
            }

            return accountHistory;
        }
    }
}
