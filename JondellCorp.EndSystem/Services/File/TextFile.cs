﻿using JondellCorp.EndSystem.Models;
using System.IO;

namespace JondellCorp.EndSystem.Services.File
{
    public class TextFile : File
    {
        /// <summary>
        /// Read text file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public override AccountHistory ReadFile(FileUploadModel model)
        {
            AccountHistory accountHistory = new AccountHistory
            {
                UploadedFileName = model.FileName,
                UploadedFileType = model.ContentType
            };

            // TODO : File conent/format validation
            using (StreamReader reader = new StreamReader(model.FileStream))
            {
                // TODO : Improve reading by account name without reading from file record order
                string firstLine = reader.ReadLine();
                accountHistory.RD = decimal.Parse(reader.ReadLine().Split('\t')[1].ToString());
                accountHistory.Canteen = decimal.Parse(reader.ReadLine().Split('\t')[1].ToString());
                accountHistory.CEOCar = decimal.Parse(reader.ReadLine().Split('\t')[1].ToString());
                accountHistory.Marketing = decimal.Parse(reader.ReadLine().Split('\t')[1].ToString());
                accountHistory.ParkingFines = decimal.Parse(reader.ReadLine().Split('\t')[1].ToString());
            }

            return accountHistory;
        }
    }
}
