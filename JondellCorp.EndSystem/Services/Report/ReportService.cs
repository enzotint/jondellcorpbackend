﻿using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.Report
{
    public class ReportService : IReportService
    {
        private IJondellCorpDbContext _jondelCorpDbContext;
        private ILog _logger;

        /// <summary>
        /// Constructor dependancy injection
        /// </summary>
        /// <param name="jondelCorpDbContext"></param>
        public ReportService(IJondellCorpDbContext jondelCorpDbContext, ILog logger)
        {
            _jondelCorpDbContext = jondelCorpDbContext;
            _logger = logger;
        }

        public async Task<List<AccountHistory>> GetReportAsync(string fromYear, string toYear, string fromMonth, string toMonth)
        {
            try
            {
                DateTime fromDate = DateTime.Parse("" + fromYear + "-" + fromMonth + "-01");
                DateTime toDate = DateTime.Parse("" + toYear + "-" + toMonth + "-01");

                List<AccountHistory> result = await _jondelCorpDbContext.AccountHistory.Where(x => x.AsignedDate >= fromDate && x.AsignedDate <= toDate)
                    .OrderBy(x => x.AsignedDate).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("report error : ", ex);
                // TODO : Exception Handle
                throw;
            }
        }
    }
}
