﻿using JondellCorp.EndSystem.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.Report
{
    public interface IReportService
    {
        Task<List<AccountHistory>> GetReportAsync(string fromYear, string toYear, string fromMonth, string toMonth);
    }
}
