﻿using JondellCorp.EndSystem.Models;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.Overview
{
    public interface IOverviewService
    {
        Task<AccountHistory> GetAccountAsync(string year, string month);
    }
}
