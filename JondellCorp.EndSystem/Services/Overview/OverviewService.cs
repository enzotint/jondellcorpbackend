﻿using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Models;
using log4net;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.Overview
{
    public class OverviewService : IOverviewService
    {
        private IJondellCorpDbContext _jondelCorpDbContext;
        private ILog _logger;

        /// <summary>
        /// Constructor dependancy injection
        /// </summary>
        /// <param name="jondelCorpDbContext"></param>
        public OverviewService(IJondellCorpDbContext jondelCorpDbContext, ILog logger)
        {
            _jondelCorpDbContext = jondelCorpDbContext;
            _logger = logger;
        }

        /// <summary>
        /// Return account information
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public async Task<AccountHistory> GetAccountAsync(string year, string month)
        {
            AccountHistory result = null;
            try
            {
                Month selectedMonth = (Month)Enum.Parse(typeof(Month), month);
                int selectedYear = int.Parse(year);

                return await _jondelCorpDbContext.AccountHistory.Where(x => x.Year == selectedYear && x.Month == (int)selectedMonth).FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("overview error : ", ex);
                // TODO : Exception Handle
                return result;
            }
        }
    }
}
