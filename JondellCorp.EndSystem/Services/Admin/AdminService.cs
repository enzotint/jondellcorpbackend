﻿using JondellCorp.EndSystem.Common;
using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Models;
using JondellCorp.EndSystem.Services.File;
using log4net;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace JondellCorp.EndSystem.Services.Admin
{
    public class AdminService : IAdminService
    {
        private IJondellCorpDbContext _jondelCorpDbContext;
        private ILog _logger;

        /// <summary>
        /// Constructor dependancy injection
        /// </summary>
        /// <param name="jondelCorpDbContext"></param>
        public AdminService(IJondellCorpDbContext jondelCorpDbContext, ILog logger)
        {
            _jondelCorpDbContext = jondelCorpDbContext;
            _logger = logger;
        }

        /// <summary>
        /// Process file and insert into database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string ProcessFile(FileUploadModel model)
        {
            try
            {
                var file = FileFactory.Get(EnumExchange.GetValueFromDescription<FileType>(model.ContentType));
                AccountHistory accountHistory = file.ReadFile(model);


                accountHistory.Year = int.Parse(model.Year);
                accountHistory.Month = (int)model.Month;
                accountHistory.AsignedDate = DateTime.Parse("" + accountHistory.Year + "-" + accountHistory.Month + "-01");

                using (TransactionScope transaction = new TransactionScope())
                {
                    // Check is exists
                    AccountHistory selectedRecord = _jondelCorpDbContext.AccountHistory.Where(x => x.Year == accountHistory.Year && x.Month == accountHistory.Month).FirstOrDefault();

                    // Save into account history
                    if (selectedRecord == null)
                    {
                        accountHistory.UploadedDateTime = DateTime.Now;
                        accountHistory.UploadedUser = model.UserName;
                        accountHistory.LastUpdatedDateTime = DateTime.Now;
                        accountHistory.UpdatedUser = model.UserName;

                        var accountHistoryRecord = _jondelCorpDbContext.AccountHistory.Add(accountHistory);
                    }
                    else
                    {
                        selectedRecord.RD = accountHistory.RD;
                        selectedRecord.Canteen = accountHistory.Canteen;
                        selectedRecord.CEOCar = accountHistory.CEOCar;
                        selectedRecord.Marketing = accountHistory.Marketing;
                        selectedRecord.ParkingFines = accountHistory.ParkingFines;
                        selectedRecord.UploadedFileName = accountHistory.UploadedFileName;
                        selectedRecord.UploadedFileType = accountHistory.UploadedFileType;
                        selectedRecord.LastUpdatedDateTime = DateTime.Now;
                        accountHistory.UpdatedUser = model.UserName;
                    }

                    _jondelCorpDbContext.SaveChanges();
                    transaction.Complete();
                }
                return "Success";
            }
            catch (Exception ex)
            {
                _logger.Error("file read error : ", ex);
                return "Error reading file";
                // TODO : Exception Handle
            }
        }
    }
}
