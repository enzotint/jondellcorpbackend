﻿using JondellCorp.EndSystem.Models;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.Admin
{
    public interface IAdminService
    {
        string ProcessFile(FileUploadModel model);
    }
}
