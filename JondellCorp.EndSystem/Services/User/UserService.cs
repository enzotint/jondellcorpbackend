﻿using JondellCorp.EndSystem.DataContext;
using JondellCorp.EndSystem.Models;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.User
{
    public class UserService : IUserService
    {
        private IJondellCorpDbContext _jondelCorpDbContext;
        private ILog _logger;

        /// <summary>
        /// Constructor dependancy injection
        /// </summary>
        /// <param name="jondelCorpDbContext"></param>
        public UserService(IJondellCorpDbContext jondelCorpDbContext, ILog logger)
        {
            _jondelCorpDbContext = jondelCorpDbContext;
            _logger = logger;
        }

        /// <summary>
        /// Create new user on identity
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Identity Result</returns>
        public async Task<IdentityResult> CreateUserAsync(UserModel model)
        {
            try
            {
                var userStore = new UserStore<ApplicationUser>(_jondelCorpDbContext.Create());
                var userManager = new UserManager<ApplicationUser>(userStore);
                var newUser = new ApplicationUser() { UserName = model.UserName, Email = model.Email };

                IdentityResult result = await userManager.CreateAsync(newUser, model.Password);
                userManager.AddToRoles(newUser.Id, model.Roles);
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("user error : ", ex);
                // TODO : Exception Handle
                throw;
            }
        }

        /// <summary>
        /// Get user roles
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Identity Result</returns>
        public async Task<List<UserRoleModel>> GetUserRolesAsync()
        {
            try
            {
                var roleStore = new RoleStore<IdentityRole>(_jondelCorpDbContext.Create());
                var roleMngr = new RoleManager<IdentityRole>(roleStore);

                var result = await roleMngr.Roles
                    .Select(x => new UserRoleModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("user error : ", ex);
                // TODO : Exception Handle
                throw;
            }
        }
    }
}
