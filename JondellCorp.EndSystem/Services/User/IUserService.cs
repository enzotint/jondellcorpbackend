﻿using JondellCorp.EndSystem.Models;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace JondellCorp.EndSystem.Services.User
{
    public interface IUserService
    {
        Task<IdentityResult> CreateUserAsync(UserModel model);
        Task<List<UserRoleModel>> GetUserRolesAsync();
    }
}
