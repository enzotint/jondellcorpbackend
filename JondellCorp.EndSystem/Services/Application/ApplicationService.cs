﻿using JondellCorp.EndSystem.DataContext;
using log4net;
using System;
using System.Linq;

namespace JondellCorp.EndSystem.Services.Application
{
    public class ApplicationService : IApplicationService
    {
        private IJondellCorpDbContext _jondelCorpDbContext;
        private ILog _logger;

        /// <summary>
        /// Constructor dependancy injection
        /// </summary>
        /// <param name="jondelCorpDbContext"></param>
        public ApplicationService(IJondellCorpDbContext jondelCorpDbContext, ILog logger)
        {
            _jondelCorpDbContext = jondelCorpDbContext;
            _logger = logger;
        }

        /// <summary>
        /// Apllication service test method
        /// </summary>
        /// <returns>application master data</returns>
        public JondellCorp.EndSystem.Models.Application GetApplication()
        {
            JondellCorp.EndSystem.Models.Application application = null;
            try
            {
                application = _jondelCorpDbContext.Application.FirstOrDefault();
                return application;
            }
            catch (Exception ex)
            {
                _logger.Error("application error : ", ex);
                // TODO : Exception Handle
                return application;
            }
        }
    }
}
