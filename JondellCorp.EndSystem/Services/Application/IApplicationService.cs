﻿namespace JondellCorp.EndSystem.Services.Application
{
    public interface IApplicationService
    {
        JondellCorp.EndSystem.Models.Application GetApplication();
    }
}
