namespace JondellCorp.EndSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M005AccountHistorychangeaddcoulmn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountHistory", "AsignedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccountHistory", "AsignedDate");
        }
    }
}
