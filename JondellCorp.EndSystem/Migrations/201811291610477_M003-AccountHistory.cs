namespace JondellCorp.EndSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M003AccountHistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountHistory",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Year = c.Int(nullable: false),
                        Month = c.Int(nullable: false),
                        RD = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Canteen = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CEOCar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Marketing = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ParkingFines = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UploadedDateTime = c.DateTime(nullable: false),
                        UploadedUser = c.String(),
                        LadtUpdatedDateTime = c.DateTime(nullable: false),
                        UpdatedUser = c.String(),
                        UploadedFileType = c.String(),
                        UploadedFileName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AccountHistory");
        }
    }
}
