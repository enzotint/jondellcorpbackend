namespace JondellCorp.EndSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M003AccountHistorychange : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountHistory", "LastUpdatedDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.AccountHistory", "LadtUpdatedDateTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountHistory", "LadtUpdatedDateTime", c => c.DateTime(nullable: false));
            DropColumn("dbo.AccountHistory", "LastUpdatedDateTime");
        }
    }
}
