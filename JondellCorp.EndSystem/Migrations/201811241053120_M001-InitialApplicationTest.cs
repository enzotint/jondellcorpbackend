namespace JondellCorp.EndSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M001InitialApplicationTest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Application",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Application");
        }
    }
}
