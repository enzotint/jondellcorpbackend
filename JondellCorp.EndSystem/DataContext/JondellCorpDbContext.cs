﻿using JondellCorp.EndSystem.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace JondellCorp.EndSystem.DataContext
{
    public class JondellCorpDbContext : IdentityDbContext<ApplicationUser>, IJondellCorpDbContext
    {
        /// <summary>
        /// Databse Connection
        /// </summary>
        public JondellCorpDbContext() : base("LocalConnection", throwIfV1Schema: false) { }

        /// <summary>
        /// DTO(s)
        /// </summary>
        public virtual DbSet<Application> Application { get; set; }

        public virtual DbSet<AccountHistory> AccountHistory { get; set; }

        /// <summary>
        /// Remove Pluralizing table names
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
            //AspNetUsers to User
            modelBuilder.Entity<ApplicationUser>().ToTable("User");

            //AspNetRoles to Role
            modelBuilder.Entity<IdentityRole>().ToTable("Role");

            //AspNetUserRoles to UserRole
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");

            //AspNetUserClaims to UserClaim
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");

            //AspNetUserLogins to UserLogin
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
        }

        /// <summary>
        /// Initiate context
        /// </summary>
        /// <returns></returns>
        public JondellCorpDbContext Create()
        {
            return new JondellCorpDbContext();
        }
    }
}
