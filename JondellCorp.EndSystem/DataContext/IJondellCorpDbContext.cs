﻿using JondellCorp.EndSystem.Models;
using System.Data.Entity;

namespace JondellCorp.EndSystem.DataContext
{
    public interface IJondellCorpDbContext
    {
        JondellCorpDbContext Create();

        DbSet<Application> Application { get; set; }

        DbSet<AccountHistory> AccountHistory { get; set; }

        int SaveChanges();
    }
}
