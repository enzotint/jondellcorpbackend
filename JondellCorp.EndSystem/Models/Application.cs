﻿using System.ComponentModel.DataAnnotations;

namespace JondellCorp.EndSystem.Models
{
    public class Application
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
