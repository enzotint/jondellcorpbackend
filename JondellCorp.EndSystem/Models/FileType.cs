﻿using System.ComponentModel;

namespace JondellCorp.EndSystem.Models
{
    public enum FileType
    {
        [Description("text/plain")] text = 1,
        [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")] excel = 2,
    }
}
