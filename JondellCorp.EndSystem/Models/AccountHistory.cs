﻿using System;
using System.ComponentModel.DataAnnotations;

namespace JondellCorp.EndSystem.Models
{
    public class AccountHistory
    {
        [Key]
        public int Id { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }

        public decimal RD { get; set; }

        public decimal Canteen { get; set; }

        public decimal CEOCar { get; set; }

        public decimal Marketing { get; set; }

        public decimal ParkingFines { get; set; }

        public DateTime UploadedDateTime { get; set; }

        public string UploadedUser { get; set; }

        public DateTime LastUpdatedDateTime { get; set; }

        public string UpdatedUser { get; set; }

        public string UploadedFileType { get; set; }

        public string UploadedFileName { get; set; }

        public DateTime AsignedDate { get; set; }
    }
}
