﻿namespace JondellCorp.EndSystem.Models
{
    public class UserRoleModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}
