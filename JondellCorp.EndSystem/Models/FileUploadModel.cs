﻿using System.IO;

namespace JondellCorp.EndSystem.Models
{
    public class FileUploadModel
    {
        public string Year { get; set; }

        public Month Month { get; set; }

        public Stream FileStream { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }

        public string UserName { get; set; }
    }
}
